package com.luxoft.gui_swt;

/**
 * Functional interface. Doesn't accept parameters. Returns void
 * 
 * @author Georgiy Kucherenko
 *
 */
@FunctionalInterface
public interface Procedure {
	
	/**
	 * Method to be performed. No parameters. Returns void
	 */
	void perform();
}
