package com.luxoft.gui_swt;

import java.util.Arrays;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.*;
import com.luxoft.calculator.models.*;
import com.luxoft.calculator.service.CalculatorService;
import com.luxoft.calculator.utils.ArithmeticOperation;


/**
 * This class builds SWT-based GUI for Calculator. 
 * It extends org.eclipse.swt.widgets.Composite. 
 * It doesn't have any public fields or methods. Only public constructor.
 *  
 * @author Georgiy Kucherenko
 *
 */
public class SwtGuiCalculator extends Composite {
	private static final String CALCULATOR = "Calculator";
	private static final String HISTORY = "History";
	private static final String DEVIDED_BY_ZERO = "Looks like you divided by zero";
	private static final String INTERNAL_ERROR = "Internal error happened. Try again";
	private static final String NOT_ACTIVE_OPERATION = "This operation is not active";
	private final CalculatorService calculator;
	private CalculatorComposite calculatorComposite;
	private HistoryComposite historyComposite;

	/**
	 * Public constructor
	 * 
	 * @param shell - parent shell
	 * @param calculatorService - instance of CalculatorService API
	 */
	public SwtGuiCalculator(Shell shell, CalculatorService calculatorService) {
		super(shell, SWT.NONE);
		this.calculator = calculatorService;
		
		setLayout(new FillLayout());
		setupAllTabs();
	}

	private void setupAllTabs() {
		TabFolder tabFolder = new TabFolder(this, SWT.NONE);
		
		setupCalculatorTab(tabFolder);
		setupHistoryTab(tabFolder);
	}

	private void setupCalculatorTab(TabFolder tabFolder) {
		TabItem calculatorTab = new TabItem(tabFolder, SWT.BORDER);

		calculatorTab.setText(CALCULATOR);

		calculatorComposite = new CalculatorComposite(tabFolder, this::calculate);
		calculatorTab.setControl(calculatorComposite);
	}

	private void setupHistoryTab(TabFolder tabFolder) {
		TabItem historyTab = new TabItem(tabFolder, SWT.BORDER);

		historyTab.setText(HISTORY);
		
		historyComposite = new HistoryComposite(tabFolder);
		historyTab.setControl(historyComposite);
	}

	private void calculate() {
		try {
			ResultOfCalculation result = calculator.getResultOfCalculation(
					Double.parseDouble(calculatorComposite.getValueInputText1()),
					Double.parseDouble(calculatorComposite.getValueInputText2()),
					getArithmeticOperation(calculatorComposite.getValueOperationsCombo()));

			calculatorComposite.setValueResultText(calculator.getRoundedResult(result.getResult()));
			historyComposite.addToHistoryList(calculator.getRoundedResult(result.getResult()), 0);
		} catch (ArithmeticException a) {
			calculatorComposite.setValueResultText(DEVIDED_BY_ZERO);
			historyComposite.addToHistoryList(DEVIDED_BY_ZERO, 0); 
		} catch (Exception e) {
			calculatorComposite.setValueResultText(e.getMessage());
			historyComposite.addToHistoryList(INTERNAL_ERROR, 0);
		}
	}

	private ArithmeticOperation getArithmeticOperation(String selectedOperation) throws UnsupportedOperationException {
		return Arrays.stream(ArithmeticOperation.values())
				.filter(a -> a.getTitle().equals(selectedOperation))
				.findFirst()
				.orElseThrow(() -> new UnsupportedOperationException(NOT_ACTIVE_OPERATION));
	}
}
