package com.luxoft.calculator.utils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class holds all possible types of Arithmetic operations that can
 * be used in {@link org.example.calculator.service.CalculatorService}.
 * Each type of operation is represented by its String title also. This Title is used for GUI and logs.
 */
public enum ArithmeticOperation {
    /**
     * Operation of summarising
     */
    SUM(" + "),
    /**
     * Operation of deducting
     */
    MINUS(" - "),
    /**
     * Operation of multiplying
     */
    MULTIPLE(" * "),
    /**
     * Operation of dividing
     */
    DIVIDE(" / ");

    private final String title;

    ArithmeticOperation(String s) {
        title = s;
    }

    public String getTitle() {
        return title;
    }
    
    public static String[] getArrayOfOperationsTitles() {
		ArithmeticOperation[] arrayOfOperations = values();
		List<String> listOfOperationsTitles = Arrays.stream(arrayOfOperations)
				.map(ArithmeticOperation::getTitle).collect(Collectors.toList());
		String[] arrayOfOperationsTitles = new String[listOfOperationsTitles.size()];
		listOfOperationsTitles.toArray(arrayOfOperationsTitles);
		return arrayOfOperationsTitles;
	}
}
