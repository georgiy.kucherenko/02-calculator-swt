package com.luxoft.calculator.arithmetic_processor_impl;

import com.luxoft.calculator.models.ArithmeticProcessor;
import com.luxoft.calculator.models.ResultOfCalculation;

/**
 * The class implements interface {@link ArithmeticProcessor}.
 * It provides the only one method of subtraction one double from another double.
 *
 * @author Georgiy Kucherenko
 */
public class MinusArithmeticProcessorImpl implements ArithmeticProcessor {
    @Override
    public ResultOfCalculation calculate(Double a, Double b, String describingOfOperation) {
        return new ResultOfCalculation(a - b, a, b, describingOfOperation);
    }
}
