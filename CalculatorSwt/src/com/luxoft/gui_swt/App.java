package com.luxoft.gui_swt;

import java.util.Map;

import com.luxoft.calculator.arithmetic_processor_impl.*;
import com.luxoft.calculator.service.CalculatorService;
import com.luxoft.calculator.utils.ArithmeticOperation;

import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.*;


/**
 * Main class for starting GUI-Calculator.
 * Working on SWT-basis.
 * 
 * @author Georgiy Kucherenko
 *
 */
public class App {
	private static final String OUTPUT_FORMAT = "#0.000";
	private static final String CALCULATOR = "Calculator";

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		CalculatorService calculator = createCalculatorService();
		tuneShell(shell);
		new SwtGuiCalculator(shell, calculator);
		
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

	private static CalculatorService createCalculatorService() {
		return new CalculatorService(
				Map.of(
						ArithmeticOperation.SUM, new SumArithmeticProcessorImpl(),
						ArithmeticOperation.DIVIDE, new DivideArithmeticProcessorImpl(),
						ArithmeticOperation.MULTIPLE, new MultipleArithmeticProcessorImpl(), 
						ArithmeticOperation.MINUS, new MinusArithmeticProcessorImpl()
						), 
				OUTPUT_FORMAT);
	}
	
	private static void tuneShell(Shell shell) {
		shell.setSize(500, 500);
		shell.setMinimumSize(400, 400);
		shell.setLayout(new FillLayout());
		shell.setText(CALCULATOR);

		configureShellPosition(shell);
	}

	private static void configureShellPosition(Shell shell) {
		Monitor primary = shell.getDisplay().getPrimaryMonitor();
		Rectangle bounds = primary.getBounds();
		Rectangle rect = shell.getBounds();

		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;

		shell.setLocation(x, y);
	}
}
