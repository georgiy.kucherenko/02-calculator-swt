package com.luxoft.calculator.arithmetic_processor_impl;

import com.luxoft.calculator.models.ArithmeticProcessor;
import com.luxoft.calculator.models.ResultOfCalculation;

/**
 * The class  implements interface {@link  ArithmeticProcessor}.
 * It provides the only one method of dividing one double by another double.
 *
 * @author Georgiy Kucherenko
 */
public class DivideArithmeticProcessorImpl implements ArithmeticProcessor {
    @Override
    public ResultOfCalculation calculate(Double a, Double b, String describingOfOperation) {
        if (b != 0) {
            return new ResultOfCalculation(a / b, a, b, describingOfOperation);
        } else {
            throw new ArithmeticException(String.format("Cannot divide by zero.. you have entered %f / %f", a, b));
        }
    }
}
