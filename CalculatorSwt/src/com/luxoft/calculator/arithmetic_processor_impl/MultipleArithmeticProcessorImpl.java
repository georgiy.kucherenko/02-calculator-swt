package com.luxoft.calculator.arithmetic_processor_impl;

import com.luxoft.calculator.models.ResultOfCalculation;
import com.luxoft.calculator.models.ArithmeticProcessor;

/**
 * The class implements interface {@link ArithmeticProcessor}.
 * It provides the only one method of multiplying one double and another double.
 *
 * @author Georgiy Kucherenko
 */
public class MultipleArithmeticProcessorImpl implements ArithmeticProcessor {
    @Override
    public ResultOfCalculation calculate(Double a, Double b, String describingOfOperation) {
        return new ResultOfCalculation(a * b, a, b, describingOfOperation);
    }
}
