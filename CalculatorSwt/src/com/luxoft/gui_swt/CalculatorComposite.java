package com.luxoft.gui_swt;

import java.util.regex.*;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import com.luxoft.calculator.utils.ArithmeticOperation;

/**
 * Instance of this class is actually a part of SwtGuiCalculator. 
 * It represents the content of main Calculator's tab and
 * also provides listeners, initiate calculation operations,
 * shows the results of operations and manipulates the data from second tab.
 * 
 * @author Georgiy Kucherenko
 *
 */
public class CalculatorComposite extends Composite {
	private static final String RESULT = "Result:";
	private static final String CALCULATE = "Calculate";
	private static final String CALCULATE_ON_FLY = "Calculate on fly";
	private static Pattern pattern = Pattern.compile("^-?\\d+\\.\\d*$|^-?\\d+$");
	private Combo operationsCombo;
	private Text inputText1;
	private Text inputText2;
	private Text resultText;
	private Button onFlyCheckBox;
	private Button calculateButton;

	/**
	 * Public constructor 
	 * 
	 * @param composite - Parent composite (Instance of TabFolder)
	 * @param calculateAction link to Calculate method 
	 */
	public CalculatorComposite(Composite composite, Procedure calculateAction) {
		super(composite, SWT.NONE);
		
		setLayout(new FillLayout());
		setupSashForm();
		setupListeners(calculateAction);
	}
	
	private void setupSashForm() {
		final SashForm sashForm = new SashForm(this, SWT.VERTICAL | SWT.BORDER);
		addTopSashChild(sashForm);
		addButtomSashChild(sashForm);
		sashForm.setWeights(new int[] { 3, 7 });
	}

	private void addTopSashChild(SashForm sashForm) {
		Composite topComposite = createSashComposite(sashForm, 3);
		addInputText1(topComposite);
		addOperationsCombo(topComposite);
		addInputText2(topComposite);
	}
	
	private void addButtomSashChild(SashForm sashForm) {
		Composite bottomComposite = createSashComposite(sashForm, 3);
		addOnFlyCheckBox(bottomComposite);
		addCalculateButton(bottomComposite);
		addInfoLabel(bottomComposite);
		addResultText(bottomComposite);
	}

	private Composite createSashComposite(SashForm sashForm, int cols){
		final Composite sashComposite = new Composite(sashForm, SWT.NONE);
		sashComposite.setLayout(new GridLayout(cols, false));
		return sashComposite;
	}
	
	private void addInputText1(Composite topComposite) {
		inputText1 = new Text(topComposite, SWT.SINGLE | SWT.BORDER);
		inputText1.setLayoutData(createTopCompositeGridData());
	}
	
	private void addOperationsCombo(Composite topComposite) {
		operationsCombo = new Combo(topComposite, SWT.READ_ONLY | SWT.CENTER);
		operationsCombo.setItems(ArithmeticOperation.getArrayOfOperationsTitles());
		operationsCombo.select(0);
		operationsCombo.setLayoutData(createTopCompositeGridData());
	}	
	
	private void addInputText2(Composite topComposite) {
		inputText2 = new Text(topComposite, SWT.SINGLE | SWT.BORDER);
		inputText2.setLayoutData(createTopCompositeGridData());
	}
	
	private GridData createTopCompositeGridData () {
		GridData gData = new GridData(GridData.FILL_BOTH);
		gData.verticalAlignment=SWT.BEGINNING;
		return gData;
	}

	private void addResultText(Composite bottomComposite) {
		resultText = new Text(bottomComposite, SWT.SINGLE | SWT.BORDER | SWT.RIGHT);
		resultText.setEnabled(false);
		GridData gData = new GridData(GridData.FILL_HORIZONTAL);
		gData.horizontalSpan = 2;
		resultText.setLayoutData(gData);
	}

	private void addInfoLabel(Composite bottomComposite) {
		final Label resultLable = new Label(bottomComposite, SWT.NONE);
		resultLable.setText(RESULT);
		resultLable.setLayoutData(new GridData());
	}

	private void addCalculateButton(Composite bottomComposite) {
		calculateButton = new Button(bottomComposite, SWT.NONE);
		calculateButton.setText(CALCULATE);
		GridData gData = new GridData(GridData.FILL_VERTICAL);
		gData.horizontalAlignment=SWT.END;
		gData.verticalAlignment=SWT.END;
		calculateButton.setLayoutData(gData);
	}

	private void addOnFlyCheckBox(Composite bottomComposite) {
		onFlyCheckBox = new Button(bottomComposite, SWT.CHECK);
		onFlyCheckBox.setText(CALCULATE_ON_FLY);
		GridData gData = new GridData(GridData.FILL_VERTICAL);
		gData.horizontalAlignment = SWT.BEGINNING;
		gData.verticalAlignment=SWT.END;
		gData.horizontalSpan = 2;
		onFlyCheckBox.setLayoutData(gData);
	}

	private void setupListeners(Procedure calculateAction) {
		inputText1.addModifyListener(createColoredModifyListener());
		inputText1.addModifyListener(createOnFlyModifyListener(calculateAction));
		inputText2.addVerifyListener(createBlockingVerifyListener());
		inputText2.addModifyListener(createOnFlyModifyListener(calculateAction));
		onFlyCheckBox.addSelectionListener(
				SelectionListener.widgetSelectedAdapter(
						e -> calculateButton.setEnabled(!onFlyCheckBox.getSelection())
						)
				);
		calculateButton.addListener(SWT.Selection, event -> calculateAction.perform());
	}

	private ModifyListener createOnFlyModifyListener(Procedure calculateAction) {
		return new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {
				if (onFlyCheckBox.getSelection()) {
					calculateAction.perform();
				}
			}
		};
	}
	
	private ModifyListener createColoredModifyListener() {
		return new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent event) {
				Text text = (Text)event.getSource();
				
				if (text.getText().isEmpty()) {
					text.setBackground(null);
					return;
				}
				
				Matcher matcher = pattern.matcher(text.getText());
				
				if (matcher.find()) {
					text.setBackground(null);
				} else {
					text.setBackground(getShell().getDisplay().getSystemColor(SWT.COLOR_GREEN));
				}
			}
		};
	}

	private VerifyListener createBlockingVerifyListener() {
		return new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent event) {
				String actual = ((Text) event.widget).getText();
				String planned = actual.substring(0, event.start) 
						+ event.text 
						+ actual.substring(event.end, actual.length());
				
				if (!pattern.matcher(planned).find() && !planned.isEmpty()) {
					event.doit=false;
				}
			}
		};
	}

	/**
	 * Provides access to one field of inputText2
	 * @return inputText2.getText()
	 */
	public String getValueInputText2() {
		return inputText2.getText();
	}

	/**
	 * Provides access to one field of operationsCombo
	 * @return operationsCombo.getText()
	 */
	public String getValueOperationsCombo() {
		return operationsCombo.getText();
	}
	
	/**
	 * Sets string to resultText field. 
	 * @param roundedResult - String to be set
	 */
	public void setValueResultText(String roundedResult) {
		resultText.setText(roundedResult);
	}

	/**
	 * Provides access to one field of inputText2
	 * @return inputText1.getText()
	 */
	public String getValueInputText1() {
		return inputText1.getText();
	}
}