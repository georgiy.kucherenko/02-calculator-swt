package com.luxoft.calculator.models;

/**
 * This is an interface for all arithmetic operations that are used
 * in {@link org.example.calculator.service.CalculatorService}.
 * It provides the only one method for implementation of arithmetic operation.
 *
 * @author Georgiy Kucherenko
 * @see org.example.calculator.service.CalculatorService
 */
public interface ArithmeticProcessor {

    /**
     * This method performs an arithmetic operation for {@link org.example.calculator.service.CalculatorService}
     *
     * @param a                     the first numeric in arithmetic operation to be applied
     * @param b                     the second numeric in arithmetic operation to be applied
     * @param describingOfOperation string representation of arithmetic operation (example: " + ", " - ", etc).
     *                              All possible values are listed
     *                              in {@link org.example.calculator.utils.ArithmeticOperation}
     * @return {@link ResultOfCalculation} class that encapsulates results of arithmetic operation
     * @throws ArithmeticException in case of dividing by zero
     */
    ResultOfCalculation calculate(Double a, Double b, String describingOfOperation) throws ArithmeticException;
}
