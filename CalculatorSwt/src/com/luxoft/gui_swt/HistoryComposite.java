package com.luxoft.gui_swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.*;

/**
 * Instance of this class is actually a part of SwtGuiCalculator. 
 * It represents the content of History's Calculator tab
 *  
 * @author Georgiy Kucherenko
 *
 */
public class HistoryComposite extends Composite {
	private List historyList;

	/**
	 * Public constructor
	 * 
	 * @param composite - Parent composite (Instance of TabFolder)
	 */
	public HistoryComposite(Composite composite) {
		super(composite, SWT.NONE);
		
		setLayout(new FillLayout());
		historyList = new List(this, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL);
	}

/**
 * Method for adding information to history list
 * 
 * @param roundedResult - string to be added (result of calculation or error message)
 * @param index - position in list to be added at
 */
	public void addToHistoryList(String roundedResult, int index) {
		historyList.add(roundedResult, index);
	}
}
